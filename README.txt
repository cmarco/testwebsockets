FUNCIONAMIENTO

Uso: chat.exe [puerto]

El programa tiene dos modos: cliente o servidor; éste entrará en modo servidor si el puerto TCP indicado está libre para escucha, y en modo cliente si no lo está.

El servidor tiene por tarea mantener un listado de clientes justo con sus nicks escogidos, y mostrar los mensajes que se mandan los usuarios.
Siempre que un cliente entre en el chat o lo abandone, el servidor mostrará y enviará un mensaje indicando quién se conectó o desconectó.

El cliente tiene por tarea pedir un nombre al usuario con el cual será representado en el servidor, y acto seguido, conectarse al servidor para chatear.
Al conectarse, recibirá un mensaje de bienvenida y podrá mandar mensajes al servidor, para que el resto de usuario conectados lo lean.


CÓDIGO

Todo el código ha sido escrito y comentado en inglés para facilitar su reusabilidad (nunca sabes quién tendrá que leerlo).
En cuanto a inyección de dependencias, no he usado ningún framework ya que me parece un proyecto bastante simple como para compliarlo de esta forma, así que he optado por construir yo mismo cualquier dependencia necesaria.
Para evitar errores de conexión al cerrar las ventanas de consola, he añadido un manejador para capturar el evento de cierre.
En el método principal del programa se crean los servicios de servidor o de cliente, dependiendo del modo. Ambos implementan la interfaz IChatService, que incluye un método Close(). Esto me permite llamar al método Close() desde el manejador de cierre de consola independientemente del servicio (servidor o cliente) que se haya creado, y gestionar las finalizaciones pertinentes de cada servicio.
También he incluído una clase Constants para poder almacenar literales con el fin de facilitar su traducción si posteriormente fuera necesario. Esto también permite leerlos desde cualquier parte de la aplicación, y en caso de necesitar un cambio, no es necesario navegar por todo el código.

He encontrado un problema a la hora de iniciar el servidor Fleck en un puerto ya usado, ya que el programador de la librería espera que eso se maneje a nivel de sistema operativo, por lo que he optado por la solución propuesta por otro usuario en el siguiente pull request del proyecto:
https://github.com/statianzo/Fleck/pull/280

Para la conexión a nivel cliente, he usado el socket nativo de .NET, ya que el wrapper propuesto por Fleck no me suponía ninguna nueva utilidad y me conplicaba un poco la escritura y lectura.