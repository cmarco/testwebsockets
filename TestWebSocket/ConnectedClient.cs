﻿using Fleck;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestWebSocket
{
    /// <summary>
    /// Represents a client connection to the chat server.
    /// </summary>
    public class ConnectedClient
    {
        /// <summary>
        /// Gets or sets the connection socket.
        /// </summary>
        public IWebSocketConnection Socket { get; set; }

        /// <summary>
        /// Gets or sets the nickname of the person who connected.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Creates a new client connection.
        /// </summary>
        /// <param name="name">Name of the person connecting.</param>
        /// <param name="socket">Connection socket.</param>
        public ConnectedClient(string name, IWebSocketConnection socket)
        {
            this.Socket = socket;
            this.Name = name;
        }
    }
}
