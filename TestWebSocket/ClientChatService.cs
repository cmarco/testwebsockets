﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestWebSocket
{
    /// <summary>
    /// Represents a service for create and manage a chat client.
    /// </summary>
    public class ClientChatService : IChatService
    {
        /// <summary>
        /// Gets the name of the client.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the socket connection to the server.
        /// </summary>
        public ClientWebSocket Client { get; private set; }

        /// <summary>
        /// Gets or sets if the client is ready for reading.
        /// </summary>
        private bool _readingFlag;

        /// <summary>
        /// Creates a new chat client.
        /// </summary>
        /// <param name="name">Name client wants to be known.</param>
        /// <param name="port">Port where to connect.</param>
        public ClientChatService(string name, int port)
        {
            this._readingFlag = false;
            this.Name = name;
            this.Client = new ClientWebSocket();
            this.Client.ConnectAsync(new Uri("ws://localhost:" + port), CancellationToken.None).Wait();
        }

        /// <summary>
        /// Starts the client.
        /// </summary>
        public void Start()
        {
            string msg = $"/name {this.Name}";
            ArraySegment<byte> bytesToSend = new ArraySegment<byte>(Encoding.UTF8.GetBytes(msg));
            Task sending = this.Client.SendAsync(bytesToSend, WebSocketMessageType.Text, true, CancellationToken.None);
            sending.Wait();

            Thread reading = new Thread(new ThreadStart(ReceiveMessages));
            reading.Start();
            Console.WriteLine();
            Console.WriteLine(string.Format(Constants.WELCOME_MSG, this.Name));
            do
            {
                msg = Console.ReadLine();
                if (Constants.EXIT_COMMANDS.Contains(msg) == false
                    && this._readingFlag)
                {
                    bytesToSend = new ArraySegment<byte>(Encoding.UTF8.GetBytes(string.Format(Constants.SPEAK_MSG, this.Name, msg)));
                    sending = this.Client.SendAsync(bytesToSend, WebSocketMessageType.Text, true, CancellationToken.None);
                    sending.Wait();
                }
            } while (Constants.EXIT_COMMANDS.Contains(msg) == false
                    && this._readingFlag);

            this.Close();
        }

        /// <summary>
        /// Closes the connection to the server.
        /// </summary>
        public void Close()
        {
            if (this.Client != null && this.Client.CloseStatus.HasValue == false)
            {
                this._readingFlag = false;
                this.Client.CloseAsync(WebSocketCloseStatus.Empty, string.Empty, CancellationToken.None);
            }
        }

        /// <summary>
        /// Loop method to read data from the server.
        /// </summary>
        private async void ReceiveMessages()
        {
            this._readingFlag = true;
            while (this._readingFlag)
            {
                ArraySegment<byte> bytesReceived = new ArraySegment<byte>(new byte[1024]);
                try
                {
                    WebSocketReceiveResult result = await this.Client.ReceiveAsync(bytesReceived, CancellationToken.None);
                    Console.WriteLine(Encoding.UTF8.GetString(bytesReceived.Array, 0, result.Count));
                }catch(Exception)
                {
                    this._readingFlag = false;
                }
            }
        }
    }
}
