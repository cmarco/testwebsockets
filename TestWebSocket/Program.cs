﻿using Fleck;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestWebSocket
{
    class Program
    {
        private static IChatService _chatService;

        /*
         * Handler to manage console closing
         * and close connections properly to avoid errors
         * and maintain communication to clients.
         */
        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(SetConsoleCtrlEventHandler handler, bool add);
        private delegate bool SetConsoleCtrlEventHandler(CtrlType sig);

        static void Main(string[] args)
        {
            Fleck.FleckLog.Level = LogLevel.Error;

            int port = Constants.DEFAULT_PORT;
            if (args.Length > 0 && Int32.TryParse(args[0], out port) == false)
            {
                Console.WriteLine("Use: chat [port]");
                Console.WriteLine();
                return;
            }

            SetConsoleCtrlHandler(Handler, true);

            if (!CheckIfPortAvailable(port))
            {
                Console.WriteLine($"(Start on port {port})");
                DoClientMode(port);
            }
            else
            {
                Console.WriteLine($"(Start on port {port})");
                DoServerMode(port);
            }
        }

        /// <summary>
        /// Checks if TCP port is available por server websocket.
        /// </summary>
        /// <param name="port">Port number.</param>
        /// <returns>True if port is available for listening, false otherwise.</returns>
        /// <remarks>This is a solution commented in 'https://github.com/statianzo/Fleck/pull/280'</remarks>
        public static bool CheckIfPortAvailable(int port)
        {
            IPGlobalProperties properties = IPGlobalProperties.GetIPGlobalProperties();
            IPEndPoint[] end_point = properties.GetActiveTcpListeners();
            var ports = end_point.Select(p => p.Port).ToList<int>();

            return !ports.Contains(port);
        }

        /// <summary>
        /// Creates and starts a new chat server.
        /// </summary>
        /// <param name="port">Port for listening.</param>
        public static void DoServerMode(int port)
        {
            Console.WriteLine(@" ~ SERVER MODE ~ ");
            Console.WriteLine();

            _chatService = new ServerChatService(port);
            _chatService.Start();
        }

        /// <summary>
        /// Creates and stats a new chat client.
        /// </summary>
        /// <param name="port">Port where to connect.</param>
        public static void DoClientMode(int port)
        {
            Console.Write(Constants.CHOOSE_NAME_MSG);
            string name = string.Empty;
            do
            {
                name = Console.ReadLine().Split(' ').First();
            } while (string.IsNullOrWhiteSpace(name));

            _chatService = new ClientChatService(name, port);
            _chatService.Start();
        }

        /// <summary>
        /// Handles the console closing.
        /// </summary>
        private static bool Handler(CtrlType signal)
        {
            if(_chatService != null )
            {
                _chatService.Close();
            }
            return true;
        }
    }
}
