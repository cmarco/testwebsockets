﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestWebSocket
{
    /// <summary>
    /// Stores needed constants and static variables to use in the application.
    /// </summary>
    public static class Constants
    {
        public const int DEFAULT_PORT = 8181;
        public const string CHOOSE_NAME_MSG = "Choose a chat name for you (no spaces): ";
        public const string WELCOME_MSG = "{0}, welcome to the chat!";
        public const string JOIN_MSG = "{0} has joined to chat.";
        public const string LEAVE_MSG = "{0} has left the chat.";
        public const string SPEAK_MSG = "[{0}]: {1}";
        public const string SERVER_CLOSING_MSG = "Server is closing...";

        public static readonly string[] EXIT_COMMANDS = { "/quit", "/exit", "/q" };
    }

    /// <summary>
    /// Represents console closing signal values.
    /// </summary>
    public enum CtrlType
    {
        CTRL_C_EVENT = 0,
        CTRL_BREAK_EVENT = 1,
        CTRL_CLOSE_EVENT = 2,
        CTRL_LOGOFF_EVENT = 5,
        CTRL_SHUTDOWN_EVENT = 6
    }
}
