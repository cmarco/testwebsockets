﻿using Fleck;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestWebSocket
{
    /// <summary>
    /// Represents a service for create and manage a chat server.
    /// </summary>
    public class ServerChatService : IChatService
    {
        /// <summary>
        /// Gets the server socket.
        /// </summary>
        public WebSocketServer Server { get; private set; }

        /// <summary>
        /// Gets the list of connected clients.
        /// </summary>
        public List<ConnectedClient> Clients { get; private set; }

        /// <summary>
        /// Creates a new chat server.
        /// </summary>
        /// <param name="port">Port to listen.</param>
        public ServerChatService(int port)
        {
            this.Server = new WebSocketServer($"ws://127.0.0.1:{port}");
            this.Clients = new List<ConnectedClient>();
        }

        /// <summary>
        /// Starts the server.
        /// </summary>
        public void Start()
        {
            this.Server.Start(socket =>
            {
                socket.OnMessage = (message) => ManageMessage(socket, message);
                socket.OnClose = () => ManageClose(socket);
            });

            string command = string.Empty;
            do
            {
                command = Console.ReadLine();

            } while (Constants.EXIT_COMMANDS.Contains(command) == false);
        }

        /// <summary>
        /// Closes server and all current client connections.
        /// </summary>
        public void Close()
        {
            this.BoradcastMessage(null, Constants.SERVER_CLOSING_MSG);
            foreach (var c in this.Clients)
            {
                c.Socket.Close();
            }
        }

        /// <summary>
        /// Manages a new message sent by a client.
        /// </summary>
        /// <param name="socket">Client who sent the message.</param>
        /// <param name="message">Message sent by the client.</param>
        private void ManageMessage(IWebSocketConnection socket, string message)
        {
            if (message.StartsWith("/name"))
            {
                string name = message.Split(' ').Last();
                this.Clients.Add(new ConnectedClient(name, socket));
                this.BoradcastMessage(socket, string.Format(Constants.JOIN_MSG, name));
            }
            else
            {
                this.BoradcastMessage(socket, message);
            }
        }

        /// <summary>
        /// Broadcasts a message to all connected clients.
        /// </summary>
        /// <param name="socket">Client who sends the message.</param>
        /// <param name="message">Message to send.</param>
        private void BoradcastMessage(IWebSocketConnection socket, string message)
        {
            Console.WriteLine(message);
            foreach (var c in this.Clients)
            {
                if (!c.Socket.Equals(socket))
                {
                    c.Socket.Send(message);
                }
            }
        }

        /// <summary>
        /// Manages the connection closing of a client.
        /// </summary>
        /// <param name="socket">Client who is closing connection.</param>
        private void ManageClose(IWebSocketConnection socket)
        {
            ConnectedClient client = this.Clients.Where(c => c.Socket == socket).FirstOrDefault();
            if(client != null )
            {
                this.Clients.Remove(client);
                this.BoradcastMessage(socket, string.Format(Constants.LEAVE_MSG, client.Name));
            }
        }
    }
}
